package com.yhy.admin.vo.mng;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-23 下午9:48 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

import com.yhy.common.jxls.MyJxlsImportBean;

public class CpyImportVO extends MyJxlsImportBean {
    private String cpyName;
    private String cpyCode;

    public String getCpyName() {
        return cpyName;
    }

    public void setCpyName(String cpyName) {
        this.cpyName = cpyName;
    }

    public String getCpyCode() {
        return cpyCode;
    }

    public void setCpyCode(String cpyCode) {
        this.cpyCode = cpyCode;
    }
}
