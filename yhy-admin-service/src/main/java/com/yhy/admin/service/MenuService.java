package com.yhy.admin.service;

import com.yhy.admin.dao.MenuDao;
import com.yhy.admin.vo.MenuVO;
import com.yhy.common.service.BaseMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional(rollbackFor = Exception.class)
public class MenuService extends BaseMainService<MenuVO> {


    @Autowired
    private MenuDao menuDao;

    @Override
    protected MenuDao getDao() {
        return menuDao;
    }


    public List<MenuVO> findAllNotAdminMenu() {
        return getDao().findAllNotAdminMenu();
    }

}
