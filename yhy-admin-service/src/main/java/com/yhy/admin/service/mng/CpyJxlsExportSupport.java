package com.yhy.admin.service.mng;

import com.yhy.admin.dto.CpyDTO;
import com.yhy.admin.vo.mng.CpyMngVO;
import com.yhy.common.jxls.MyJxlsExportSupport;
import com.yhy.common.utils.QrBarcodeUtil;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import org.jxls.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-23 下午4:14 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Service("cpyJxlsExportSupport")
public class CpyJxlsExportSupport extends MyJxlsExportSupport<CpyDTO> {
    @Autowired
    private CpyMngService cpyMngService;

    @Override
    protected CpyMngService getBaseService() {
        return cpyMngService;
    }

    @Override
    public Map<String, Object> processDatas(CpyDTO baseDto) {
        SysUser sysUser = YhyUtils.getSysUser();
        sysUser.setCpyCode(null);
        baseDto.setSysUser(sysUser);
        Map<String, Object> rtnMap = super.processDatas(baseDto);
        List<CpyMngVO> cpyMngVOList = (List<CpyMngVO>) rtnMap.get("rtnList");
        for (CpyMngVO mngVO : cpyMngVOList) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            QrBarcodeUtil.getBarcodeWriteByte(YhyUtils.genRandom(),null, null, byteArrayOutputStream);
            byte[] imageBytes = byteArrayOutputStream.toByteArray();
            mngVO.setImg(imageBytes);
            try {
                byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return rtnMap;
    }

}
