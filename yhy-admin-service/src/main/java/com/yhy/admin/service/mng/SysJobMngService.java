package com.yhy.admin.service.mng;

import com.yhy.admin.dao.SysJobMngDao;
import com.yhy.admin.dto.SysJobDTO;
import com.yhy.admin.service.SysJobService;
import com.yhy.admin.service.SysUserJobService;
import com.yhy.admin.vo.SysUserJobVO;
import com.yhy.admin.vo.mng.SysJobMngVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseMngService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午1:56 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysJobMngService extends BaseMngService<SysJobDTO,SysJobMngVO> {

    @Autowired
    private SysJobMngDao sysJobMngDao;

    @Autowired
    private SysJobService sysJobService;
    @Autowired
    private SysUserJobService userJobService;

    @Override
    protected SysJobMngDao getBaseMngDao() {
        return sysJobMngDao;
    }

    @Override
    public String getBusModule() {
        return BusModuleType.MD_SYS_JOB_MNG.getVal();
    }

    @Override
    public SysJobService getBaseMainService() {
        return sysJobService;
    }

    @Override
    protected void processOfToAdd(SysJobDTO baseDTO) {
        super.processOfToAdd(baseDTO);
        baseDTO.getBusMainData().setEnableFlag("Y");
    }

    @Override
    protected void beforeDeleteOfProcess(SysJobMngVO mngVO) {
        SysUserJobVO paramBean = new SysUserJobVO();
        paramBean.setJobId(mngVO.getId());
        paramBean.setEnableFlag("Y");
        if (userJobService.hasBy(paramBean)) {
            throw new BusinessException(mngVO.getJobName() + "该职务下有用户不能删除.");
        }
    }

}
