package com.yhy.admin.service.mng;

import com.yhy.admin.dao.DataDictMngDao;
import com.yhy.admin.dto.DataDictDTO;
import com.yhy.admin.service.DataDictService;
import com.yhy.admin.vo.DataDictVO;
import com.yhy.admin.vo.mng.DataDictMngVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.service.BaseMngService;
import com.yhy.common.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-13 下午4:24 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-26 下午3:04 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DataDictMngService extends BaseMngService<DataDictDTO,DataDictMngVO> {

    @Autowired
    private DataDictMngDao dataDictMngDao;

    @Autowired
    private DataDictService dataDictService;


    @Override
    public DataDictService getBaseMainService() {
        return dataDictService;
    }

    @Override
    protected DataDictMngDao getBaseMngDao() {
        return dataDictMngDao;
    }

    @Override
    public String getBusModule() {
        return BusModuleType.MD_DATA_DICT_MNG.getVal();
    }


    public List<DataDictMngVO> getDictByType(String dictType,String lanCode) {
        List<DataDictVO> rtnVO = dataDictService.getDictByType(dictType,lanCode);
        return JsonUtils.tranList(rtnVO,DataDictMngVO.class);
    }

    @Override
    protected void processOfToAdd(DataDictDTO baseDTO) {
        super.processOfToAdd(baseDTO);
        baseDTO.getBusMainData().setEnableFlag("Y");
        baseDTO.getBusMainData().setLanCode("zh_CN");
    }
}
