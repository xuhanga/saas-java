package com.yhy.admin.dao;

import com.yhy.admin.vo.OrgSetVO;
import com.yhy.common.dao.BaseDao;
import com.yhy.common.dao.BaseMainDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */
@Mapper
@Component(value = "orgSetDao")
public interface OrgSetDao extends BaseMainDao<OrgSetVO> {


}
