package com.yhy.common.token;

import java.util.HashMap;
import java.util.Map;

public enum TokenState {
	 /**
	  * 过期
	  */
	EXPIRED("EXPIRED"),
	/**
	 * 无效(token不合法)
	 */
	INVALID("INVALID"), 
	/**
	 * 有效的
	 */
	VALID("VALID");  
	
    private String  state;

	private static Map<String, TokenState> tokenStateMap;

	private TokenState(String state) {
        this.state = state;  
    }
    
	public static TokenState getInstByVal(String val) {
		if(tokenStateMap == null) {
			synchronized (TokenState.class) {
				if (tokenStateMap == null) {
					tokenStateMap = new HashMap<String, TokenState>();
					for (TokenState state : TokenState.values()) {
						tokenStateMap.put(state.getState(), state);
					}
				}
			}
		}
		return tokenStateMap.get(val);
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
    
}  
