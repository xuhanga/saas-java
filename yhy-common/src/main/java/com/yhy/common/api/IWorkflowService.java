package com.yhy.common.api;

import com.yhy.common.constants.BusSelfState;
import com.yhy.common.dto.workflow.WorkflowInfo;

import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-5-12 上午11:36 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public interface IWorkflowService {

    String SERVICE_BEAN = "workflowService";
    /*
    * 工作流启动，用于单据提交
    *  返回状态
     */
    BusSelfState submitWorkflow(String userAccount, WorkflowInfo workflowInfo, Map<String, Object> variables);

}
