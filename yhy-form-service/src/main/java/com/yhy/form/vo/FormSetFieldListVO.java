package com.yhy.form.vo;

import com.yhy.common.dto.BaseEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-15 下午2:22 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class FormSetFieldListVO extends BaseEntity {
	
    /**
     * 主表ID db_column: MAIN_ID 
     */	
	private String mainId;
    /**
     * 控件ID db_column: FIELD_CONTROL_ID 
     */	
	private String fieldControlId;
    /**
     * 控件名称 db_column: VALUE 
     */	
	private String value;
    /**
     * 控件默认值 db_column: LABEL 
     */	
	private String label;
    /**
     * 列表选项 db_column: PARENT_VALUE 
     */	
	private String parentValue;

	public FormSetFieldListVO(){
	}

	public FormSetFieldListVO( String mainId ){
		this.mainId = mainId;
	}

	public String getMainId() {
		return this.mainId;
	}
	
	public void setMainId(String mainId) {
		this.mainId = mainId;
	}
	public String getFieldControlId() {
		return this.fieldControlId;
	}
	
	public void setFieldControlId(String fieldControlId) {
		this.fieldControlId = fieldControlId;
	}
	public String getValue() {
		return this.value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	public String getLabel() {
		return this.label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	public String getParentValue() {
		return this.parentValue;
	}
	
	public void setParentValue(String parentValue) {
		this.parentValue = parentValue;
	}


}

