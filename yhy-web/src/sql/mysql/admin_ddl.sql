/*==============================================================*/
/* Table: t_sys_data_dict                                       */
/*==============================================================*/
CREATE TABLE t_sys_data_dict
(
   id                   VARCHAR(40) NOT NULL COMMENT '主键(sys_code+UUID)',
   dict_type            VARCHAR(30) NOT NULL COMMENT '类别',
   lan_code             VARCHAR(10) COMMENT '上级菜单',
   dict_code            VARCHAR(30) COMMENT '前台URL路径',
   dict_name            VARCHAR(100) COMMENT '后台访问路径',
   sort                 INT COMMENT '排序',
   create_date          datetime NOT NULL,
   last_update_date     datetime COMMENT '最后更新时间',
   create_by            VARCHAR(40),
   last_update_by       VARCHAR(40) COMMENT '最后更新人',
   sys_org_code         VARCHAR(40) COMMENT '用户所属组织部门',
   sys_owner_cpy        VARCHAR(40) COMMENT '用户所属公司',
   rmk                  VARCHAR(1000) COMMENT '备注',
   enable_flag          CHAR(1) COMMENT '有效标识',
   attribute1           VARCHAR(100) COMMENT '扩展属性1 ',
   attribute2           VARCHAR(100) COMMENT '扩展属性二',
   VERSION              INT(8) COMMENT '更新版本号',
   attribute3           VARCHAR(100),
   attribute4           VARCHAR(100),
   attribute5           VARCHAR(100),
   attribute6           VARCHAR(100),
   attribute7           VARCHAR(100),
   attribute8           VARCHAR(100),
   PRIMARY KEY (id),
   UNIQUE INDEX bus_common_state_idxh_mainid (DICT_TYPE, DICT_CODE, LAN_CODE)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

ALTER TABLE t_sys_data_dict COMMENT '数据字典表';

/*==============================================================*/
/* Table: t_sys_cpy                                             */
/*==============================================================*/

/*==============================================================*/
/* Table: t_sys_cpy                                             */
/*==============================================================*/
create table t_sys_cpy
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   cpy_name             varchar(100) not null comment '组织名称',
   cpy_long_name        varchar(200) comment '完整名称',
   cpy_type             varchar(10) not null comment '类别(公司，部门，员工)',
   cpy_code             varchar(100) comment '组织编号（系统ID+自动编码）',
   cer_code             varchar(100) comment '主要证书编号',
   owner_app            varchar(10) comment '所属应用app',
   cpy_status           varchar(10) comment '公司状态',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   SYS_ORG_CODE         VARCHAR(40) comment '用户所属组织部门',
   SYS_OWNER_CPY        VARCHAR(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   primary key (id),
   INDEX sys_cpy_idxh_cpycode (cpy_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_cpy comment '公司';

/*==============================================================*/
/* Table: t_sys_user                                            */
/*==============================================================*/
create table t_sys_user
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   user_name            varchar(100) not null comment '用户名',
   user_account         varchar(40) not null comment '账号',
   password             varchar(40) comment '密码(密码+用户名 MD5加密串)',
   phone                varchar(100) comment '手机号',
   email                varchar(100) comment '邮件地址',
   lan_code             varchar(10) comment '默认语言',
   cpy_code        varchar(40) comment '所属组织部门ID',
   org_code        varchar(40) comment '所属组织部门编码',
   avatar               varchar(200) comment '头像',
   weixin               varchar(100) comment '微信',
   user_status          varchar(20) comment '用户状态(Lock:锁定,UnLock:解锁)',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   third_account        varchar(50) comment '第三方账号',
   primary key (id),
   INDEX sys_user_idxh_account (USER_ACCOUNT)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_user comment '系统用户';
create unique index sys_user_u01_useraccount on t_sys_user
(
   user_account
);
/*==============================================================*/
/* Table: t_sys_user_mgr                                        */
/*==============================================================*/
CREATE TABLE t_sys_user_mgr
(
   id                   VARCHAR(40) NOT NULL COMMENT '主键(sys_code+UUID)',
   user_id              VARCHAR(40) NOT NULL COMMENT '用户ID',
   user_account         VARCHAR(40) NOT NULL COMMENT '账号',
   mgr_account          VARCHAR(40) COMMENT '上级用户账号',
   cpy_code             VARCHAR(40) COMMENT '公司 编号',
   create_date          datetime NOT NULL,
   last_update_date     datetime COMMENT '最后更新时间',
   create_by            VARCHAR(40),
   last_update_by       VARCHAR(40) COMMENT '最后更新人',
   sys_org_code         VARCHAR(40) COMMENT '用户所属组织部门',
   sys_owner_cpy        VARCHAR(40) COMMENT '用户所属公司',
   rmk                  VARCHAR(1000) COMMENT '备注',
   enable_flag          CHAR(1) COMMENT '有效标识',
   attribute1           VARCHAR(100) COMMENT '扩展属性1 ',
   attribute2           VARCHAR(100) COMMENT '扩展属性二',
   VERSION              INT(8) COMMENT '更新版本号',
   PRIMARY KEY (id),
   INDEX sys_user_mgr_idx_account (user_account,cpy_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

ALTER TABLE t_sys_user_mgr COMMENT '用户对应上司';

/*==============================================================*/
/* Table: t_sys_user_cpy                                        */
/*==============================================================*/
create table t_sys_user_cpy
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   user_id              varchar(40) not null comment '用户ID',
   user_account         varchar(40) not null comment '账号',
   cpy_code             varchar(40) comment '公司 ID（对应组织ID）',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   primary key (id),
   INDEX sys_user_cpy_idx_mainid (user_account,cpy_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_user_cpy comment '用户所属公司';

/*==============================================================*/
/* Table: t_sys_org_set                                         */
/*==============================================================*/
create table t_sys_org_set
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   org_name             varchar(200) not null comment '组织名称',
   org_type             varchar(10) not null comment '类别(公司，部门，员工)',
   parent_id            varchar(40) comment '上级菜单',
   org_code             varchar(100) comment '组织编号（系统ID+自动编码）',
   org_status           varchar(2) comment '组织状态',
   cpy_code             varchar(40),
   third_code           varchar(50) comment '第三方编号',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   mgr_account          varchar(50) comment '当前部门负责人',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   SYS_ORG_CODE         varchar(40) comment '用户所属组织',
   primary key (id),
   INDEX sys_org_set_idx_orgcode(org_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_org_set comment '组织机构(部门)';

/*==============================================================*/
/* Table: t_sys_user_org_set                                    */
/*==============================================================*/
create table t_sys_user_org_set
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   user_id              varchar(40) not null comment '用户ID',
   user_account         varchar(40) not null comment '账号',
   org_code             varchar(100) comment '组织代码(通常对应公司部门代码 ）',
   cpy_code             varchar(40) comment '公司 ID（对应组织ID）',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   primary key (id),
   INDEX sys_user_org_set_idx_account (user_account),
   INDEX sys_user_org_set_idx_orgcode (org_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_user_org_set comment '用户所属组织部门';

/*==============================================================*/
/* Table: t_sys_menu                                            */
/*==============================================================*/
create table t_sys_menu
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   menu_name            varchar(200) not null comment '菜单名称',
   menu_type            varchar(10) not null comment '类别(外部，内部)',
   parent_id            varchar(40) comment '上级菜单',
   component_url        varchar(100) comment '组件路径',
   path_url             varchar(100) comment '链接地址',
   sort                 int(3) comment '排序号',
   icon                 varchar(100) comment '菜单图标',
   is_admin             char(1) comment '是否管理员',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   module_bus_class     varchar(20) comment '业务单据Code',
   is_visable           char(1) comment '菜单是否可见',
   is_workflow          char(1) comment '是否开启工作流',
   priv_code            varchar(50) comment '权限标识',
   primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_menu comment '菜单管理';

/*==============================================================*/
/* Table: t_sys_menu_lan                                        */
/*==============================================================*/
create table t_sys_menu_lan
(
   id                   varchar(40),
   menu_id              varchar(40) not null comment '菜单主ID',
   lan_code             varchar(10) not null comment '菜单名称',
   name                 varchar(100) not null comment '类别',
   rmk                  varchar(1000) comment '备注',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   primary key (menu_id, lan_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_menu_lan comment '菜单管理多语言';

/*==============================================================*/
/* Table: t_quartz_job                                          */
/*==============================================================*/
create table t_quartz_job
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   job_name             varchar(100) not null comment 'job名称',
   cron_expression      varchar(50) comment '表达式',
   is_pause             int(1) comment '是否暂停(0:暂停,1:运行)',
   is_running           int(1) comment '是否正在执行(1:执行,0:未执行)',
   bean_name            varchar(100),
   method_param         varchar(200),
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_quartz_job comment '定时任务表';

/*==============================================================*/
/* Table: t_sys_job                                             */
/*==============================================================*/
CREATE TABLE t_sys_job
(
   id                   VARCHAR(40) NOT NULL COMMENT '主键(sys_code+UUID)',
   job_name             VARCHAR(200) NOT NULL COMMENT '组织名称',
   job_type             VARCHAR(10) NOT NULL COMMENT '类别(公司，部门，员工)',
   job_code             VARCHAR(100) COMMENT '组织编号（系统ID+自动编码）',
   job_status           VARCHAR(2) COMMENT '组织状态',
   cpy_code             VARCHAR(40),
   third_code           VARCHAR(50) COMMENT '第三方编号',
   sys_org_code         VARCHAR(40) COMMENT '用户所属组织部门',
   sys_owner_cpy        VARCHAR(40) COMMENT '用户所属公司',
   create_date          datetime NOT NULL,
   last_update_date     datetime COMMENT '最后更新时间',
   create_by            VARCHAR(40),
   last_update_by       VARCHAR(40) COMMENT '最后更新人',
   rmk                  VARCHAR(1000) COMMENT '备注',
   enable_flag          CHAR(1) COMMENT '有效标识',
   attribute1           VARCHAR(100) COMMENT '扩展属性1 ',
   attribute2           VARCHAR(100) COMMENT '扩展属性二',
   VERSION              INT(8) COMMENT '更新版本号',
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

ALTER TABLE t_sys_job COMMENT '职务管理';

/*==============================================================*/
/* Table: t_sys_user_job                                        */
/*==============================================================*/
create table t_sys_user_job
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   user_id              varchar(40) not null comment '用户ID',
   job_name             varchar(100) not null comment '职务名称',
   job_id               varchar(40) comment '职位ID',
   cpy_code             varchar(40) comment '公司编号',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   primary key (id),
   INDEX sys_user_job_idx_userid (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;
alter table t_sys_user_job comment '用户对应职位';

/*==============================================================*/
/* Table: t_sys_print_main                                      */
/*==============================================================*/
create table t_sys_print_main
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   report_code          varchar(100) comment '报表编号(对应系统数据字典的print_report_type)',
   report_name          varchar(100) comment '报表名称',
   attachment_id        varchar(40) comment '附档ID对应 t_sys_attachment.id',
   attachment_path      varchar(200) comment '报表sql',
   report_sort          int(3) comment '排序',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   primary key (id),
   INDEX sys_print_main_idx_code (sys_owner_cpy,report_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_print_main comment '自定义报表打印格式';

/*==============================================================*/
/* Table: t_sys_role                                            */
/*==============================================================*/
CREATE TABLE t_sys_role
(
   id                   VARCHAR(40) NOT NULL COMMENT '主键(sys_code+UUID)',
   role_name            VARCHAR(100) NOT NULL COMMENT '用户名',
   create_date          datetime NOT NULL,
   last_update_date     datetime COMMENT '最后更新时间',
   create_by            VARCHAR(40),
   last_update_by       VARCHAR(40) COMMENT '最后更新人',
   sys_org_code         VARCHAR(40) COMMENT '用户所属组织部门',
   sys_owner_cpy        VARCHAR(40) COMMENT '用户所属公司',
   rmk                  VARCHAR(1000) COMMENT '备注',
   enable_flag          CHAR(1) COMMENT '有效标识',
   attribute1           VARCHAR(100) COMMENT '扩展属性1 ',
   attribute2           VARCHAR(100) COMMENT '扩展属性二',
   VERSION              INT(8) COMMENT '更新版本号',
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

ALTER TABLE t_sys_role COMMENT '角色表(RBAC Role-Base-Access-Control)';

/*==============================================================*/
/* Table: t_sys_role_user                                       */
/*==============================================================*/
CREATE TABLE t_sys_role_user
(
   id                   VARCHAR(40) NOT NULL COMMENT '主键(sys_code+UUID)',
   role_id              VARCHAR(40) NOT NULL COMMENT '角色 ID',
   user_id              VARCHAR(40) COMMENT '用户ID',
   user_account         VARCHAR(40) COMMENT '用户账号',
   create_date          datetime NOT NULL,
   last_update_date     datetime COMMENT '最后更新时间',
   create_by            VARCHAR(40),
   last_update_by       VARCHAR(40) COMMENT '最后更新人',
   sys_org_code         VARCHAR(40) COMMENT '用户所属组织部门',
   sys_owner_cpy        VARCHAR(40) COMMENT '用户所属公司',
   rmk                  VARCHAR(1000) COMMENT '备注',
   enable_flag          CHAR(1) COMMENT '有效标识',
   attribute1           VARCHAR(100) COMMENT '扩展属性1 ',
   attribute2           VARCHAR(100) COMMENT '扩展属性二',
   VERSION              INT(8) COMMENT '更新版本号',
   PRIMARY KEY (id),
   INDEX sys_role_user_idx_account(user_account)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

ALTER TABLE t_sys_role_user COMMENT '角色用户表';

/*==============================================================*/
/* Table: t_sys_role_menu                                       */
/*==============================================================*/
create table t_sys_role_menu
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   role_id              varchar(40) not null comment '角色 ID',
   menu_id              varchar(40) comment '菜单ID(对应下级菜单ID)',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   primary key (id),
   index sys_role_menu_menuid(menu_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_role_menu comment '角色菜单关系表(所能操作的菜单)';

/*==============================================================*/
/* Table: t_sys_role_org                                        */
/*==============================================================*/
CREATE TABLE t_sys_role_org
(
   id                   VARCHAR(40) NOT NULL COMMENT '主键(sys_code+UUID)',
   role_id              VARCHAR(40) NOT NULL COMMENT '角色 ID',
   org_id               VARCHAR(40) COMMENT '组织ID',
   org_code             VARCHAR(40) COMMENT '组织编号',
   create_date          datetime NOT NULL,
   last_update_date     datetime COMMENT '最后更新时间',
   create_by            VARCHAR(40),
   last_update_by       VARCHAR(40) COMMENT '最后更新人',
   sys_org_code         VARCHAR(40) COMMENT '用户所属组织部门',
   sys_owner_cpy        VARCHAR(40) COMMENT '用户所属公司',
   rmk                  VARCHAR(1000) COMMENT '备注',
   enable_flag          CHAR(1) COMMENT '有效标识',
   attribute1           VARCHAR(100) COMMENT '扩展属性1 ',
   attribute2           VARCHAR(100) COMMENT '扩展属性二',
   VERSION              INT(8) COMMENT '更新版本号',
   PRIMARY KEY (id),
   INDEX sys_role_org_roleid(role_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

ALTER TABLE t_sys_role_org COMMENT '角色组织关系表(所能操作的部门)(若无则表示所有)';

/*==============================================================*/
/* Table: t_sys_role_form                                       */
/*==============================================================*/
CREATE TABLE t_sys_role_form
(
   id                   VARCHAR(40) NOT NULL COMMENT '主键(sys_code+UUID)',
   role_id              VARCHAR(40) NOT NULL COMMENT '角色 ID',
   form_code            VARCHAR(40) COMMENT '表单系统编号',
   form_name            VARCHAR(40) COMMENT '表单名称',
   create_date          DATETIME NOT NULL,
   last_update_date     DATETIME COMMENT '最后更新时间',
   create_by            VARCHAR(40),
   last_update_by       VARCHAR(40) COMMENT '最后更新人',
   sys_org_code         VARCHAR(40) COMMENT '用户所属组织部门',
   sys_owner_cpy        VARCHAR(40) COMMENT '用户所属公司',
   rmk                  VARCHAR(1000) COMMENT '备注',
   enable_flag          CHAR(1) COMMENT '有效标识',
   attribute1           VARCHAR(100) COMMENT '扩展属性1 ',
   attribute2           VARCHAR(100) COMMENT '扩展属性二',
   VERSION              NUMERIC(8,0) COMMENT '更新版本号',
   PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

ALTER TABLE t_sys_role_form COMMENT '角色表单关系表';

/*==============================================================*/
/* Index: sys_role_form_idx_roleid                              */
/*==============================================================*/
CREATE INDEX sys_role_form_idx_roleid ON t_sys_role_form
(
   role_id
);

/*==============================================================*/
/* Table: t_sys_custom_field                                    */
/*==============================================================*/
create table t_sys_custom_field
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   grid_id              varchar(40) comment '表单ID',
   field_prop           varchar(40) comment '列字段名',
   field_label          varchar(60) comment '列名称',
   field_width          numeric(4,0) comment '列宽度',
   field_sort           numeric(3,0) comment '字段排序',
   field_visible        numeric(1,0) comment '是否可见(1:可见，0隐藏)',
   bus_module           varchar(10) comment '模块',
   bus_class            varchar(10) comment '业务种类',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              numeric(8,0) comment '更新版本号',
   primary key (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_custom_field comment '用户自定义字段定义';

/*==============================================================*/
/* Index: sys_custom_field_idx_mainid                           */
/*==============================================================*/
create index sys_custom_field_idx_mainid on t_sys_custom_field
(
   grid_id,
   sys_owner_cpy,
   create_by,
   enable_flag
);

